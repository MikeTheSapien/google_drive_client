## Google Drive Client (Rocket)

<a href="https://crates.io/crates/rpa">![Google Drive Client Version](https://img.shields.io/badge/crates.io-v0.4.0-orange.svg?longCache=true)</a>


This library provide a way to connect to google drive API (v3) using a service account. You can make 
basic operations like upload, list, or download files. This code is meant to be used with [Rocket](https://rocket.rs/ "Rocket") server
since we can use the service account to provide access to drive without user interaction. For other use you have
another libraries like [gooogle-drive3](https://crates.io/crates/google-drive3) among others.

#### Dependencies
* [Rocket](https://rocket.rs/ "Rocket")

In your project you need to import these dependencies:

```toml
# Rocket Webserver
[dependencies.google_drive_client]
version = "0.4.0"
```

#### How to use it

You have a provider that you can get to use directly like this example:

```rust
use google_drive_client::{GoogleDriveClient, DriveFileList, DriveFileSearchBuilder};

let gdrive_client: GoogleDriveClient = GoogleDriveClient::default();
let result = gdrive_client.find(DriveFileSearchBuilder::build()
        .with_order_by("name")
        .done());
let file_list:DriveFileList = result.unwrap();
```

In this example you can see how we can get a list of files ordered by name on our drive.

Another example is using rocket, you can manage the client with rocket:
```rust
let gdrive_client: GoogleDriveClient = GoogleDriveClient::default();
rocket.manage(gdrive_client);
```
Then you can use it in a controller like this:
```rust
#[get("/storage/download_file/<file_id>")]
fn download_file(gdrive: State<GoogleDriveClient>, file_id: String) -> FileDownloadResponse {
    let storage = gdrive.inner().clone();
    storage.download_file(&file_id).unwrap()
}
```
In the last example we use ```FileDownloadResponse```, that type implements ```Responder``` from rocket, so you can just
use it directly to put the file content in the response directly, it's a clean way to resolve the response of files.

#### Setup Service Account
To use this library you need a service account from google, when you have that service account you will get a json
format service account, that contains all the private information to authenticate with gdrive. This library scan some
locations to get that file, first it checks an environment variable called ```GDRIVE_ACCOUNT_FILE_NAME``` if that
variable is present it should contain the full path location to the json file, if this variable is not present the
library continue scanning in the home directory, a service account json file with name ```service_account.json``` should be
there. If none of these options are available the library will throw an error.

#### Setup share account
Since service accounts are not allowed to own files directly because are not real accounts we need to share the files
created with this account to the real account to be able to see the files in the real account drive. To do this we share
files with the real account, to do that we use an environment variable to set that account, that variable is called ```GDRIVE_SHARE_ACCOUNT```
all files and folders created are going to be shared with this account. If this variable is not set the files are going
to be created but not shared so you will not see them in the gdrive account but the files are going to be there.
