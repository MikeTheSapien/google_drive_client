/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use reqwest::blocking::{Client, ClientBuilder};
use crate::errors::error_types::{custom_error, fail_getting_files};
use crate::handlers::token;
use crate::models::client::error::GoogleClientError;
use crate::models::server::error::GoogleError;
use crate::models::server::file::DriveFile;

const GDRIVE_FILES_PATH: &str = "https://www.googleapis.com/drive/v3/files";

pub fn handle_find(file_id: String) -> Result<Option<DriveFile>, GoogleClientError> {
    let token = token::get_token().unwrap();
    let client: Client = ClientBuilder::new().build().unwrap();
    let result = client.get(build_url(file_id).as_str())
        .header("Authorization", format!("Bearer {}", token).as_str())
        .send();
    if result.is_err() {
        Err(custom_error(&result.err().unwrap().to_string()))
    } else {
        let response = result.unwrap();
        if !response.status().is_success() {
            match response.status().as_u16() {
                404 => {
                    // File not found
                    Ok(None)
                },
                _ => {
                    info!("Error {:?}", response);
                    let google_error = response.json::<GoogleError>().unwrap();
                    Err(fail_getting_files(google_error))
                }
            }
        } else {
            let drive_file = response.json::<DriveFile>().unwrap();
            Ok(Some(drive_file))
        }
    }
}

fn build_url(file_id: String) -> String {
    format!("{}/{}", GDRIVE_FILES_PATH, file_id)
}
