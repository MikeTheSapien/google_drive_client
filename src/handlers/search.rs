/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use crate::handlers::token;
use crate::models::client::error::GoogleClientError;
use crate::errors::error_types::{fail_getting_files, custom_error};
use crate::models::client::file_search::DriveFileSearch;
use crate::models::server::file_list::DriveFileList;
use reqwest::blocking::{Client, ClientBuilder};
use crate::models::server::error::GoogleError;
use serde::export::fmt::Display;
use url::form_urlencoded::Serializer;

const GDRIVE_FILES_PATH: &str = "https://www.googleapis.com/drive/v3/files";

pub fn handle_search(search: DriveFileSearch) -> Result<DriveFileList, GoogleClientError> {
    let token = token::get_token().unwrap();
    let client: Client = ClientBuilder::new().build().unwrap();
    let result = client.get(build_url(search).as_str())
        .header("Authorization", format!("Bearer {}", token).as_str())
        .send();
    if result.is_err() {
        Err(custom_error(&result.err().unwrap().to_string()))
    } else {
        let response = result.unwrap();
        if !response.status().is_success() {
            info!("Error {:?}", response);
            let google_error = response.json::<GoogleError>().unwrap();
            Err(fail_getting_files(google_error))
        } else {
            let drive_file_list = response.json::<DriveFileList>().unwrap();
            Ok(drive_file_list)
        }
    }
}

fn build_url(search: DriveFileSearch) -> String {
    format!("{}{}", GDRIVE_FILES_PATH, build_params(search))
}

fn build_params(search: DriveFileSearch) -> String {
    let params: Vec<Option<String>> = vec![
        map_param("corpora", search.corpora),
        map_param("driveId", search.drive_id),
        map_param("fields", search.fields),
        map_param("orderBy", search.order_by),
        map_param("pageSize", search.page_size),
        map_param("pageToken", search.page_token),
        map_param("q", search.q),
        map_param("spaces", search.spaces),
    ];

    if params.len() > 0 {
        let param_values = params.iter()
            .filter(|param | param.is_some())
            .map(|param| param.clone().unwrap())
            .collect::<Vec<String>>();
        return format!("?{}", param_values.join("&"));
    } else {
        return String::new();
    }
}

fn map_param<T>(name: &str, option: Option<T>) -> Option<String> where T: Display {
    if option.is_some() {
        let value = option.unwrap().to_string();
        return Some(
            Serializer::new(String::new())
            .append_pair(name, value.as_str())
            .finish()
        );
    } else {
        return None;
    }
}