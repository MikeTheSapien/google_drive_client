/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use reqwest::blocking::{Client, ClientBuilder};
use crate::models::client::error::GoogleClientError;
use crate::handlers::token;
use crate::errors::error_types::{fail_creating_folder, fail_sharing_folder, fail_sharing_file, fail_uploading_file, custom_error, fail_trying_to_create_folder};
use crate::models::client::file_upload::{DriveFolderUpload, DriveFileUpload};
use crate::models::server::file_upload_response::FileUploadResponse;
use crate::models::server::resumable_session::{DriveResumableSessionResponse, DriveResumableSessionRequest};
use crate::models::server::permission::DrivePermission;
use crate::models::server::error::GoogleError;

const RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL: &str = "https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable";
const GDRIVE_FOLDER_MIME_TYPE: &str = "application/vnd.google-apps.folder";
const GDRIVE_SHARE_ACCOUNT: &str = "GDRIVE_SHARE_ACCOUNT";

pub fn handle_create_folder(drive_folder: &DriveFolderUpload) -> Result<String, GoogleClientError> {
    let token: String = token::get_token().unwrap();
    let client: Client = ClientBuilder::new().build().unwrap();
    let location = get_resumable_session_location_for_folder(&token, drive_folder);
    let result = client.post(location.as_str())
        .header("Content-Type", GDRIVE_FOLDER_MIME_TYPE)
        .header("Content-Length", 0)
        .send();
    if result.is_ok() {
        let response = result.unwrap();
        if response.status().is_success() {
            let share_account = dotenv::var(GDRIVE_SHARE_ACCOUNT);
            let folder_created = response.json::<DriveResumableSessionResponse>().unwrap();
            let folder_id = folder_created.id;
            if share_account.is_ok() {
                let account = share_account.unwrap();
                if share_file(account.as_str(), &folder_id, &token) {
                    return Ok(String::from(&folder_id));
                } else {
                    return Err(fail_sharing_folder(&account));
                }
            }
            return Ok(String::from(&folder_id));
        }
        let google_error: GoogleError = response.json::<GoogleError>().unwrap();
        return Err(fail_creating_folder(google_error));
    }
    Err(fail_trying_to_create_folder(&result.err().unwrap().to_string()))
}

pub fn handle_file_upload(drive_file: &DriveFileUpload) -> Result<FileUploadResponse, GoogleClientError> {
    let token = token::get_token().unwrap();
    let client: Client = ClientBuilder::new().build().unwrap();
    let session_location = get_resumable_session_location_for_file(&token, drive_file);
    let mut bytes: Vec<u8> = Vec::new();
    bytes.extend_from_slice(drive_file.bytes.as_slice());
    let file_upload_response = client.put(session_location.as_str())
        .header("Content-Length", drive_file.bytes.len().to_string())
        .header("Content-Type", drive_file.metadata.mime_type.as_str())
        .body(bytes)
        .send();
    info!("File uploaded {:?}", file_upload_response);
    if file_upload_response.is_ok() {
        let response = file_upload_response.unwrap();
        let share_account = dotenv::var(GDRIVE_SHARE_ACCOUNT);
        info!("File uploaded {:?}", response);
        if response.status().is_success() {
            let session: DriveResumableSessionResponse = response.json::<DriveResumableSessionResponse>().unwrap();
            if share_account.is_ok() {
                let account = share_account.unwrap();
                info!("File id to share {}", &session.id);
                if share_file(account.as_str(), &session.id, &token) {
                    return Ok(FileUploadResponse::from(session));
                } else {
                    return Err(fail_sharing_file(&account));
                }
            } else {
                return Ok(FileUploadResponse::from(session));
            }
        } else {
            let google_error: GoogleError = response.json::<GoogleError>().unwrap();
            return Err(fail_uploading_file(google_error));
        }
    } else {
        return Err(custom_error(&file_upload_response.err().unwrap().to_string()));
    }
}

fn get_resumable_session_location_for_file(token: &String, drive_file: &DriveFileUpload) -> String {
    let client: Client = ClientBuilder::new().build().unwrap();
    let session_request: DriveResumableSessionRequest;
    if drive_file.metadata.parent_id.is_some() {
        let parent_id = drive_file.metadata.parent_id.clone().unwrap();
        session_request = DriveResumableSessionRequest {
            name: String::from(&drive_file.metadata.file_name),
            mime_type: String::from(&drive_file.metadata.mime_type),
            description: String::from(&drive_file.metadata.description),
            parents: Some(vec![parent_id])
        };
    } else {
        session_request = DriveResumableSessionRequest {
            name: String::from(&drive_file.metadata.file_name),
            mime_type: String::from(&drive_file.metadata.mime_type),
            description: String::from(&drive_file.metadata.description),
            parents: None
        };
    }
    let resumable_session_response = client.post(RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL)
        .header("X-Upload-Content-Type", String::from(&drive_file.metadata.mime_type).as_str())
        .header("X-Upload-Content-Length", String::from(&drive_file.bytes.len().to_string()).as_str())
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Content-Length", 0)
        .header("Authorization", format!("Bearer {}", token).as_str())
        .body(json!(session_request).to_string())
        .send().unwrap();
    return resumable_session_response.headers().get("Location").unwrap().to_str().unwrap().to_string();
}

fn get_resumable_session_location_for_folder(token: &String, drive_folder: &DriveFolderUpload) -> String {
    let client: Client = ClientBuilder::new().build().unwrap();
    let mut folder_parents: Option<Vec<String>> = None;
    if drive_folder.parent_id.is_some() {
        let folder_parent_id = drive_folder.parent_id.clone().unwrap();
        folder_parents = Some(vec![folder_parent_id]);
    }
    let session_request: DriveResumableSessionRequest = DriveResumableSessionRequest {
        name: String::from(&drive_folder.name),
        mime_type: String::from(GDRIVE_FOLDER_MIME_TYPE),
        description: String::from(&drive_folder.description),
        parents: folder_parents
    };
    let resumable_session_response = client.post(RESUMABLE_FILE_UPLOAD_SESSION_REQUEST_URL)
        .header("X-Upload-Content-Type", String::from(GDRIVE_FOLDER_MIME_TYPE).as_str())
        .header("X-Upload-Content-Length", 0)
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Content-Length", 0)
        .header("Authorization", String::from("Bearer ") + token.as_str())
        .body(json!(session_request).to_string())
        .send().unwrap();
    return resumable_session_response.headers().get("Location").unwrap().to_str().unwrap().to_string();
}

fn share_file(account_email: &str, file_id: &String, token: &String) -> bool {
    let client: Client = ClientBuilder::new().build().unwrap();
    let url: String = format!("https://www.googleapis.com/drive/v3/files/{}/permissions", file_id);
    let permission_request = DrivePermission::writer(account_email);
    let permission_response = client.post(url.as_str())
        .header("Content-Type", "application/json; charset=UTF-8")
        .header("Authorization", String::from("Bearer ") + token.as_str())
        .body(json!(permission_request).to_string())
        .send();
    return if permission_response.is_ok() {
        let response = permission_response.unwrap();
        if response.status().is_success() {
            true
        } else {
            let error = response.json::<GoogleError>();
            if error.is_ok() {
                error!("Error sharing file {:?}", error);
            } else {
                error!("Unknown error sharing file");
            }
            false
        }
    } else {
        error!("Error sharing file {:?}", permission_response.err().unwrap());
        false
    }
}