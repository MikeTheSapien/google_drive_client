/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use std::error::Error;
use std::fmt::{Display, Formatter, Result};
use serde::{Serialize, Deserialize};
use crate::models::server::error::GoogleError;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GoogleClientError {
    pub description: String,
    pub cause: Option<GoogleError>
}

pub struct GoogleClientErrorBuilder {
    instance: GoogleClientError
}

impl GoogleClientErrorBuilder {
    pub fn with_description(mut self, description: &str) -> GoogleClientErrorBuilder {
        self.instance.description = description.to_string();
        self
    }
    pub fn with_cause(mut self, cause: GoogleError) -> GoogleClientErrorBuilder {
        self.instance.cause = Some(cause);
        self
    }
    pub fn done(self) -> GoogleClientError {
        self.instance
    }
}

impl GoogleClientError {
    pub fn build() -> GoogleClientErrorBuilder {
        GoogleClientErrorBuilder {
            instance: GoogleClientError {
                description: String::new(),
                cause: None
            }
        }
    }
}

impl Error for GoogleClientError {
    fn description(&self) -> &str {
        self.description.as_str()
    }
}

impl Display for GoogleClientError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.description)
    }
}