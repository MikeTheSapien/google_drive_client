/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DriveFileSearch {
    /**
        Bodies of items (files/documents) to which the query applies.
        Supported bodies are 'user', 'domain', 'drive' and 'allDrives'.
        Prefer 'user' or 'drive' to 'allDrives' for efficiency.
    **/
    pub corpora: Option<String>,
    /**
        ID of the shared drive to search.
    **/
    #[serde(rename = "driveId")]
    pub drive_id: Option<String>,
    /**
        The paths of the fields you want included in the response.
        If not specified, the response includes a default set of fields specific to this method.
        For development you can use the special value * to return all fields,
        but you'll achieve greater performance by only selecting the fields you need.
        For more information see the partial responses documentation.
    **/
    pub fields: Option<String>,
    /**
        A comma-separated list of sort keys. Valid keys are 'createdTime', 'folder',
        'modifiedByMeTime', 'modifiedTime', 'name', 'name_natural', 'quotaBytesUsed',
        'recency', 'sharedWithMeTime', 'starred', and 'viewedByMeTime'.
        Each key sorts ascending by default, but may be reversed with the 'desc' modifier.
        Example usage: ?orderBy=folder,modifiedTime desc,name.
        Please note that there is a current limitation for users with approximately one million
        files in which the requested sort order is ignored.
    **/
    #[serde(rename = "orderBy")]
    pub order_by: Option<String>,
    /**
        The maximum number of files to return per page.
        Partial or empty result pages are possible even before the end of the files list has been reached.
        Acceptable values are 1 to 1000, inclusive. (Default: 100)
    **/
    #[serde(rename = "pageSize")]
    pub page_size: Option<usize>,
    /**
        The token for continuing a previous list request on the next page.
        This should be set to the value of 'nextPageToken' from the previous response.
    **/
    #[serde(rename = "pageToken")]
    pub page_token: Option<String>,
    /**
        A query for filtering the file results. See the "Search for files" guide for the supported syntax.
    **/
    pub q: Option<String>,
    /**
        A comma-separated list of spaces to query within the corpus. Supported values are 'drive', 'appDataFolder' and 'photos'.
    **/
    pub spaces: Option<String>,
}

pub struct DriveFileSearchBuilder {
    instance: DriveFileSearch
}

impl DriveFileSearchBuilder {
    pub fn with_corpora(mut self, corpora: &str) -> DriveFileSearchBuilder {
        self.instance.corpora = Some(corpora.to_string());
        self
    }
    pub fn with_drive_id(mut self, drive_id: &str) -> DriveFileSearchBuilder {
        self.instance.drive_id = Some(drive_id.to_string());
        self
    }
    pub fn with_fields(mut self, fields: &str) -> DriveFileSearchBuilder {
        self.instance.fields = Some(fields.to_string());
        self
    }
    pub fn with_order_by(mut self, order_by: &str) -> DriveFileSearchBuilder {
        self.instance.order_by = Some(order_by.to_string());
        self
    }
    pub fn with_page_size(mut self, page_size: usize) -> DriveFileSearchBuilder {
        self.instance.page_size = Some(page_size);
        self
    }
    pub fn with_page_token(mut self, page_token: &str) -> DriveFileSearchBuilder {
        self.instance.page_token = Some(page_token.to_string());
        self
    }
    pub fn with_q(mut self, q: &str) -> DriveFileSearchBuilder {
        self.instance.q = Some(q.to_string());
        self
    }
    pub fn with_spaces(mut self, spaces: &str) -> DriveFileSearchBuilder {
        self.instance.spaces = Some(spaces.to_string());
        self
    }
    pub fn done(self) -> DriveFileSearch {
        self.instance
    }
}

impl DriveFileSearch {
    pub fn build() -> DriveFileSearchBuilder {
        DriveFileSearchBuilder {
            instance: DriveFileSearch {
                corpora: None,
                drive_id: None,
                fields: None,
                order_by: None,
                page_size: None,
                page_token: None,
                q: None,
                spaces: None,
            }
        }
    }
}