/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DriveCapabilities {
    #[serde(rename = "canEdit", skip_serializing_if = "Option::is_none")]
    pub can_edit: Option<bool>,
    #[serde(rename = "canComment", skip_serializing_if = "Option::is_none")]
    pub can_comment: Option<bool>,
    #[serde(rename = "canShare", skip_serializing_if = "Option::is_none")]
    pub can_share: Option<bool>,
    #[serde(rename = "canCopy", skip_serializing_if = "Option::is_none")]
    pub can_copy: Option<bool>,
    #[serde(rename = "canReadRevisions", skip_serializing_if = "Option::is_none")]
    pub can_read_revisions: Option<bool>,
    #[serde(rename = "canAddChildren", skip_serializing_if = "Option::is_none")]
    pub can_add_children: Option<bool>,
    #[serde(rename = "canDelete", skip_serializing_if = "Option::is_none")]
    pub can_delete: Option<bool>,
    #[serde(rename = "canDownload", skip_serializing_if = "Option::is_none")]
    pub can_download: Option<bool>,
    #[serde(rename = "canListChildren", skip_serializing_if = "Option::is_none")]
    pub can_list_children: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canMoveItemWithinDrive or canMoveItemOutOfDrive instead."
    )]
    #[serde(rename = "canMoveTeamDriveItem", skip_serializing_if = "Option::is_none")]
    pub can_move_team_drive_item: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canMoveItemOutOfDrive instead."
    )]
    #[serde(rename = "canMoveItemIntoTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_item_into_team_drive: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canReadDrive instead."
    )]
    #[serde(rename = "canReadTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_read_team_drive: Option<bool>,
    #[serde(rename = "canRemoveChildren", skip_serializing_if = "Option::is_none")]
    pub can_remove_children: Option<bool>,
    #[serde(rename = "canRename", skip_serializing_if = "Option::is_none")]
    pub can_rename: Option<bool>,
    #[serde(rename = "canTrash", skip_serializing_if = "Option::is_none")]
    pub can_trash: Option<bool>,
    #[serde(rename = "canUntrash", skip_serializing_if = "Option::is_none")]
    pub can_untrash: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "Deprecated, don't use this anymore."
    )]
    #[serde(rename = "canChangeViewersCanCopyContent", skip_serializing_if = "Option::is_none")]
    pub can_change_viewers_can_copy_content: Option<bool>,
    #[serde(rename = "canChangeCopyRequiresWriterPermission", skip_serializing_if = "Option::is_none")]
    pub can_change_copy_requires_writer_permission: Option<bool>,
    #[serde(rename = "canDeleteChildren", skip_serializing_if = "Option::is_none")]
    pub can_delete_children: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canMoveChildrenOutOfDrive instead."
    )]
    #[serde(rename = "canMoveChildrenOutOfTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_children_out_of_team_drive: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canMoveChildrenWithinDrive instead."
    )]
    #[serde(rename = "canMoveChildrenWithinTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_children_within_team_drive: Option<bool>,
    #[deprecated(
        since = "v3",
        note = "use canMoveItemWithinDrive instead."
    )]
    #[serde(rename = "canMoveItemOutOfTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_item_out_of_team_drive: Option<bool>,
    #[serde(rename = "canMoveItemWithinTeamDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_item_within_team_drive: Option<bool>,
    #[serde(rename = "canTrashChildren", skip_serializing_if = "Option::is_none")]
    pub can_trash_children: Option<bool>,
    #[serde(rename = "canMoveChildrenOutOfDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_children_out_of_drive: Option<bool>,
    #[serde(rename = "canMoveChildrenWithinDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_children_within_drive: Option<bool>,
    #[serde(rename = "canMoveItemOutOfDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_item_out_of_drive: Option<bool>,
    #[serde(rename = "canMoveItemWithinDrive", skip_serializing_if = "Option::is_none")]
    pub can_move_item_within_drive: Option<bool>,
    #[serde(rename = "canReadDrive", skip_serializing_if = "Option::is_none")]
    pub can_read_drive: Option<bool>
}