/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use openssl::sign::Signer;
use openssl::hash::MessageDigest;
use dirs::home_dir;
use std::path::Path;
use std::fs;
use base64::{encode_config, URL_SAFE_NO_PAD};
use crate::GoogleClientError;
use crate::errors::error_types::{service_account_file_not_found, reading_service_account_json};
use serde::{Serialize, Deserialize};
use chrono::prelude::*;
use openssl::pkey::PKey;
use openssl::rsa::Rsa;
use std::ops::Add;
use crate::models::server::service_account::ServiceAccount;
use chrono::Duration;

pub const SERVICE_ACCOUNT_FILE_NAME: &str = "service_account.json";
pub const SERVICE_ACCOUNT_FILE_NAME_ENV_VAR: &str = "GDRIVE_ACCOUNT_FILE_NAME";

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JWTHeader {
    pub alg: String,
    pub typ: String
}

impl JWTHeader {
    pub fn default() -> JWTHeader {
        JWTHeader {
            alg: String::from("RS256"),
            typ: String::from("JWT")
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JWTClaimSet {
    pub iss: String,
    pub scope: String,
    pub aud: String,
    pub exp: i64,
    pub iat: i64
}

impl JWTClaimSet {
    pub fn default() -> Result<JWTClaimSet, GoogleClientError> {
        let service_account: ServiceAccount = get_service_account()?;
        Ok(JWTClaimSet {
            iss: service_account.client_email,
            scope: String::from("https://www.googleapis.com/auth/drive"),
            aud: String::from("https://www.googleapis.com/oauth2/v4/token"),
            exp: Utc::now().add(Duration::minutes(30)).timestamp(),
            iat: Utc::now().timestamp()
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JWTAuthResponse {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: i64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct JWT {
    pub header: JWTHeader,
    pub header_url_safe: String,
    pub claim_set: JWTClaimSet,
    pub claim_set_url_safe: String,
    pub signature: Vec<u8>,
    pub signature_url_safe: String,
    pub jwt_encoded: String
}

impl JWT {
    pub fn new() -> Result<JWT, GoogleClientError> {
        let header: JWTHeader = JWTHeader::default();
        let header_url_safe = encode_base64_url_safe(&json!(header).to_string());
        let claim_set: JWTClaimSet = JWTClaimSet::default()?;
        let claim_set_url_safe = encode_base64_url_safe(&json!(claim_set).to_string());
        let signature = create_jwt_signature(&header_url_safe, &claim_set_url_safe)?;
        let signature_url_safe = encode_bytes_base64_url_safe(&signature);
        let jwt_encoded = format!("{}.{}.{}", &header_url_safe, &claim_set_url_safe, &signature_url_safe);
        Ok(JWT {
            header,
            header_url_safe,
            claim_set,
            claim_set_url_safe,
            signature,
            signature_url_safe,
            jwt_encoded
        })
    }
}

fn create_jwt_signature(url_safe_header: &String, url_safe_claim_set: &String) -> Result<Vec<u8>, GoogleClientError> {
    let service_account: ServiceAccount = get_service_account()?;
    let signature_input: String = format!("{}.{}", url_safe_header, url_safe_claim_set);
    let key_value = service_account.private_key.as_bytes();
    let key_pair = Rsa::private_key_from_pem(key_value).unwrap();
    let key = PKey::from_rsa(key_pair).unwrap();
    let mut signer = Signer::new(MessageDigest::sha256(), &key).unwrap();
    signer.update(signature_input.as_bytes()).unwrap();
    Ok(signer.sign_to_vec().unwrap())
}

fn get_service_account() -> Result<ServiceAccount, GoogleClientError> {
    let account: ServiceAccount;
    let service_account_path_env = dotenv::var(SERVICE_ACCOUNT_FILE_NAME_ENV_VAR);
    let service_account_path_string: String;

    if service_account_path_env.is_ok() {
        service_account_path_string = service_account_path_env.unwrap();
    } else {
        let home_dir = home_dir().unwrap();
        service_account_path_string = format!("{}/{}", home_dir.as_path().to_str().unwrap(), SERVICE_ACCOUNT_FILE_NAME);
    }

    let service_account_path = Path::new(&service_account_path_string);

    if service_account_path.exists() {
        let account_file_string = fs::read_to_string(service_account_path);
        if account_file_string.is_err() {
            return Err(reading_service_account_json())
        } else {
            account = serde_json::from_str::<ServiceAccount>(&account_file_string.unwrap().as_str()).unwrap();
        }
    } else {
        return Err(service_account_file_not_found(&String::from(SERVICE_ACCOUNT_FILE_NAME_ENV_VAR),
                                                  &String::from(SERVICE_ACCOUNT_FILE_NAME)));
    }
    Ok(account)
}

fn encode_base64_url_safe(jwt_section_json: &String) -> String {
    encode_config(jwt_section_json, URL_SAFE_NO_PAD)
}

fn encode_bytes_base64_url_safe(bytes_to_encode: &[u8]) -> String {
    encode_config(bytes_to_encode, URL_SAFE_NO_PAD)
}