/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Serialize, Deserialize};
use crate::models::client::error::{GoogleClientError};
use crate::JWTAuthResponse;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OAuth2Response {
    pub access_token: Option<String>,
    pub token_type: Option<String>,
    pub expires_in: Option<i64>,
    pub error: Option<String>,
    pub error_description: Option<String>
}

impl OAuth2Response {
    fn get_response(self: Self) -> JWTAuthResponse {
        JWTAuthResponse {
            access_token: self.access_token.unwrap(),
            expires_in: self.expires_in.unwrap(),
            token_type: self.token_type.unwrap()
        }
    }
    fn get_error(self: Self) -> GoogleClientError {
        GoogleClientError::build()
            .with_description(format!("{} -> {}", self.error_description.unwrap(), self.error.unwrap()).as_str())
            .done()
    }

    pub fn get_result(self: Self) -> Result<JWTAuthResponse, GoogleClientError> {
        if self.error.is_some() {
            return Err(self.get_error());
        }
        Ok(self.get_response())
    }
}