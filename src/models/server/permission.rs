/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Serialize, Deserialize};
use crate::models::server::permission_details::DrivePermissionDetails;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DrivePermission {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub permission_type: Option<String>,
    #[serde(rename = "emailAddress", skip_serializing_if = "Option::is_none")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub role: Option<String>,
    #[serde(rename = "allowFileDiscovery", skip_serializing_if = "Option::is_none")]
    pub allow_file_discovery: Option<bool>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "photoLink", skip_serializing_if = "Option::is_none")]
    pub photo_link: Option<String>,
    #[serde(rename = "expirationTime", skip_serializing_if = "Option::is_none")]
    pub expiration_time: Option<String>,
    #[serde(rename = "teamDrivePermissionDetails", skip_serializing_if = "Option::is_none")]
    pub team_drive_permission_details: Option<Vec<DrivePermissionDetails>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub deleted: Option<bool>,
    #[serde(rename = "permissionDetails", skip_serializing_if = "Option::is_none")]
    pub permission_details: Option<Vec<DrivePermissionDetails>>,
}

pub struct DrivePermissionBuilder {
    instance: DrivePermission
}

impl DrivePermissionBuilder {
    pub fn with_kind(mut self, kind: &str) -> DrivePermissionBuilder {
        self.instance.kind = Some(kind.to_string());
        self
    }
    pub fn with_id(mut self, id: &str) -> DrivePermissionBuilder {
        self.instance.id = Some(id.to_string());
        self
    }
    pub fn with_permission_type(mut self, permission_type: &str) -> DrivePermissionBuilder {
        self.instance.permission_type = Some(permission_type.to_string());
        self
    }
    pub fn with_email_address(mut self, email_address: &str) -> DrivePermissionBuilder {
        self.instance.email_address = Some(email_address.to_string());
        self
    }
    pub fn with_domain(mut self, domain: &str) -> DrivePermissionBuilder {
        self.instance.domain = Some(domain.to_string());
        self
    }
    pub fn with_role(mut self, role: &str) -> DrivePermissionBuilder {
        self.instance.role = Some(role.to_string());
        self
    }
    pub fn with_allow_file_discovery(mut self, allow_file_discovery: bool) -> DrivePermissionBuilder {
        self.instance.allow_file_discovery = Some(allow_file_discovery);
        self
    }
    pub fn with_display_name(mut self, display_name: &str) -> DrivePermissionBuilder {
        self.instance.display_name = Some(display_name.to_string());
        self
    }
    pub fn with_photo_link(mut self, photo_link: &str) -> DrivePermissionBuilder {
        self.instance.photo_link = Some(photo_link.to_string());
        self
    }
    pub fn with_expiration_time(mut self, expiration_time: &str) -> DrivePermissionBuilder {
        self.instance.expiration_time = Some(expiration_time.to_string());
        self
    }
    pub fn with_team_drive_permission_details(mut self, team_drive_permission_details: Vec<DrivePermissionDetails>) -> DrivePermissionBuilder {
        self.instance.team_drive_permission_details = Some(team_drive_permission_details);
        self
    }
    pub fn with_deleted(mut self, deleted: bool) -> DrivePermissionBuilder {
        self.instance.deleted = Some(deleted);
        self
    }
    pub fn with_permission_details(mut self, permission_details: Vec<DrivePermissionDetails>) -> DrivePermissionBuilder {
        self.instance.permission_details = Some(permission_details);
        self
    }
    pub fn done(self) -> DrivePermission {
        self.instance
    }
}

impl DrivePermission {
    pub fn build() -> DrivePermissionBuilder {
        DrivePermissionBuilder {
            instance: DrivePermission {
                kind: None,
                id: None,
                permission_type: None,
                email_address: None,
                domain: None,
                role: None,
                allow_file_discovery: None,
                display_name: None,
                photo_link: None,
                expiration_time: None,
                team_drive_permission_details: None,
                deleted: None,
                permission_details: None,
            }
        }
    }
    pub fn organizer(owner_address: &str) -> DrivePermission {
        DrivePermission::build()
            .with_permission_type("user")
            .with_email_address(owner_address)
            .with_role("organizer")
            .done()
    }
    pub fn file_organizer(owner_address: &str) -> DrivePermission {
        DrivePermission::build()
            .with_permission_type("user")
            .with_email_address(owner_address)
            .with_role("fileOrganizer")
            .done()
    }
    pub fn writer(owner_address: &str) -> DrivePermission {
        DrivePermission::build()
            .with_permission_type("user")
            .with_email_address(owner_address)
            .with_role("writer")
            .done()
    }
    pub fn commenter(owner_address: &str) -> DrivePermission {
        DrivePermission::build()
            .with_permission_type("user")
            .with_email_address(owner_address)
            .with_role("commenter")
            .done()
    }
    pub fn reader(owner_address: &str) -> DrivePermission {
        DrivePermission::build()
            .with_permission_type("user")
            .with_email_address(owner_address)
            .with_role("reader")
            .done()
    }
}