/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DrivePermissionDetails {
    #[deprecated(
        since = "v3",
        note = "use permissionType instead."
    )]
    #[serde(rename = "teamDrivePermissionType", skip_serializing_if = "Option::is_none")]
    pub team_drive_permission_type: Option<String>,
    #[serde(rename = "permissionType", skip_serializing_if = "Option::is_none")]
    pub permission_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub role: Option<String>,
    #[serde(rename = "inheritedFrom", skip_serializing_if = "Option::is_none")]
    pub inherited_from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inherited: Option<bool>,
}