/**
    Google Drive Client (Rocket) Library
    Copyright (C) 2019  Jonathan Franco, Hebert Vera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use serde::{Deserialize, Serialize};

use crate::handlers::{upload, download, delete, search, find};
use crate::models::client::file_upload::{DriveFileUpload, DriveFolderUpload};
use crate::models::server::file_upload_response::FileUploadResponse;
use crate::models::client::error::{GoogleClientError};
use crate::models::client::file_search::DriveFileSearch;
use crate::models::server::file_list::DriveFileList;
use crate::models::client::file_download::FileDownloadResponse;
use crate::models::server::file::DriveFile;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GoogleDriveClient {}

impl GoogleDriveClient {
    pub fn upload_file(self, drive_file: DriveFileUpload) -> Result<FileUploadResponse, GoogleClientError> {
        upload::handle_file_upload(&drive_file)
    }
    pub fn download_file(self, file_id: &String) -> Result<FileDownloadResponse, GoogleClientError> {
        download::handle_file_download(file_id)
    }
    pub fn create_folder(self, drive_folder: DriveFolderUpload) -> Result<String, GoogleClientError> {
        upload::handle_create_folder(&drive_folder)
    }
    pub fn delete_file_or_folder(self, file_or_folder_id: &String) -> Result<(), GoogleClientError> {
        delete::delete_file_or_folder(file_or_folder_id)
    }
    pub fn search(self, search: DriveFileSearch) -> Result<DriveFileList, GoogleClientError> {
        search::handle_search(search)
    }
    pub fn find(self, file_id: String) -> Result<Option<DriveFile>, GoogleClientError> {
        find::handle_find(file_id)
    }
    pub fn default() -> GoogleDriveClient {
        GoogleDriveClient::new()
    }
    fn new() -> GoogleDriveClient {
        GoogleDriveClient {}
    }
}